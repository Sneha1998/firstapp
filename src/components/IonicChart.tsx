import React from 'react';
import Chart from 'react-apexcharts';
import { useState } from 'react';
import { IonCard } from '@ionic/react';

const IonicChart: React.FC = () => {

    const [options, setOptions] = useState({
        chart: {
            id: 'realtime'
        },
        // xaxis: {
        //     categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999]
        // }
    })

    const [series, setSeries] = useState(
        [{
            name: 'series-1',
            data: [30, 40, 35, 50, 49, 60, 70, 91, 125]
        }],
    )
    return (
        <IonCard>
            <Chart options={options} series={series} type="line" height={200} />
        </IonCard>
    );
}
export default IonicChart



