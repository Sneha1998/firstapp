import React from 'react';
import {
 IonCard,
    IonCardHeader, IonCardSubtitle, IonCardTitle, IonCardContent
} from '@ionic/react';

interface CardData {
    subtitle: string,
    title: string,
    content: string
}

const Card: React.FC<{ subtitle: CardData }> = (props) => {
    console.log(props)
    return (
        <IonCard>
            <IonCardHeader>
                <IonCardSubtitle>{props.subtitle.subtitle}</IonCardSubtitle>
                <IonCardTitle>{props.subtitle.title}</IonCardTitle>
            </IonCardHeader>
            <IonCardContent>{props.subtitle.content}</IonCardContent>
        </IonCard>
    );
};
export default Card;

