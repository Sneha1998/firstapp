import React from 'react';
import {
    IonCard,
    IonGrid, IonRow, IonCol, IonText, IonContent,
} from '@ionic/react';
import './Live_Vital_Cards.css';

interface CardData {
    subtitle: string,
    img: string,
    content: string,
    text: string,
}

const Card: React.FC<{ subtitle: CardData }> = (props) => {
    console.log(props)
    return (
        <IonCard className="card">
            <IonGrid >
                <IonRow>
                    <IonCol>
                        <IonText>{props.subtitle.subtitle}</IonText>
                    </IonCol>
                    <IonCol>
                        <IonContent>{props.subtitle.img}</IonContent>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <IonText className="subcontent">{props.subtitle.content}</IonText>
                    </IonCol>
                </IonRow>
                <IonRow>
                    <IonCol>
                        <IonText className="subtext">{props.subtitle.text}</IonText>
                    </IonCol>
                </IonRow>
            </IonGrid>
        </IonCard>
            );
};

export default Card;

