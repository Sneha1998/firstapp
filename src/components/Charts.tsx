import React from 'react';
import Chart from 'react-apexcharts';
import { useState } from 'react';
import { IonCard, IonLabel } from '@ionic/react';
import  {linecolor,data}  from '../data/config.json'


const datas = data.map(x => x.data1);
console.log(datas)

// console.log(data[0]);

const Charts: React.FC = () => {
    const [option] = useState({
        chart: {
            id: 'realtime',
            toolbar: {
                show: false
            },
        },
        yaxis: {
            tooltip: {
                enabled: false
            },
            show: false,
            showAlways: false
        },
        xaxis: {
            tooltip: {
                enabled:false
            },
            labels: {
                show: false,
            },  
        },
        grid: {
            show: false
        },
        legend: {
            show: false
        }
    })

    const [series] = useState(
        [{
            // // data: [30, 40, 35, 50, 49, 60, 70, 91, 125],
            // data: data[0].data1,
            data:datas[0],
            color: linecolor
        },
        ],
    )

    return (
        <IonCard>
            <IonLabel>ECG</IonLabel>
            <Chart options={option} series={series} type="line" height={120} />
        </IonCard>
    );
}
export default Charts



