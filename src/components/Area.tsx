import ResizableBox from "../ResizableBox";
import useDemoConfig from "../useDemoConfig";
import React from "react";
import { AxisOptions, Chart } from "react-charts";
import {
    IonContent
} from '@ionic/react';


export default function Area() {
    const { data, randomizeData } = useDemoConfig({
        series: 10,
        dataType: "time",
    });

    const primaryAxis = React.useMemo<
        AxisOptions<typeof data[number]["data"][number]>
    >(
        () => ({
            getValue: (datum) => datum.primary as Date,
        }),
        []
    );

    const secondaryAxes = React.useMemo<
        AxisOptions<typeof data[number]["data"][number]>[]
    >(
        () => [
            {
                getValue: (datum) => datum.secondary,
                stacked: true,
            },
        ],
        []
    );

    return (
        <IonContent>
        <>
            <button onClick={randomizeData}>Randomize Data</button>
            <br />
            <br />
            <ResizableBox>
                <Chart
                    options={{
                        data,
                        primaryAxis,
                        secondaryAxes,
                    }}
                />
            </ResizableBox>
        </>
        </IonContent>
    );
}