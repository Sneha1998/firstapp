import { IonContent } from '@ionic/react';
import Ecg from '../Live_Vitals/Live_Vitals_Components/Charts/Ecg/Ecg'
import Header from '../Live_Vitals/Live_Vitals_Components/Headers/Headers'
import Cards from '../Live_Vitals/Live_Vitals_Components/Cards/Cards'
import Pleth from '../Live_Vitals/Live_Vitals_Components/Charts/Pleth/Pleth'
import Tabs from '../Live_Vitals/Live_Vitals_Components/Tabs/Tabs'


const Live_Vitals: React.FC = () => {
    return (
        <IonContent className="content">
            <Header/>
            <Ecg />
            <Pleth />
            <Cards />
            <IonContent>
                <Tabs />
            </IonContent>
        </IonContent>
       
    )
}

export default Live_Vitals;