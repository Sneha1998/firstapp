import React from 'react';
import Chart from 'react-apexcharts';
import { useState } from 'react';
import { IonCard, IonText, IonRow, IonCol } from '@ionic/react';
import { background, linecolor, data } from '../../../../../data/config.json'
import './Ecg.css'

const datas = data.map(x => x.data1);
console.log(datas)

// console.log(data[0]);

const Charts: React.FC = () => {
    const [option] = useState({
        chart: {
            id: 'realtime',
            toolbar: {
                show: false
            },
            background: background,
        },
        yaxis: {
            tooltip: {
                enabled: false
            },
            show: false,
            showAlways: false
        },
        xaxis: {
            tooltip: { enabled: false },
            labels: {
                show: false,
            },
        },
        grid: {
            show: false
        },
        legend: {
            show: false
        }
    })

    const [series] = useState(
        [{
            // // data: [30, 40, 35, 50, 49, 60, 70, 91, 125],
            // data: data[0].data1,
            data: datas[0],
            color: linecolor
        },
        ],
    )

    return (
        <IonCard className="content">
            <IonRow>
                <IonCol>
                    <IonText>ECG</IonText>
                </IonCol>
                <IonCol>
                    <IonText className="bpm">160<span className="bpms">bpm</span></IonText>
                </IonCol>
            </IonRow>
            
            <Chart options={option} series={series} type="line" height={80} />
        </IonCard>
    );
}
export default Charts




