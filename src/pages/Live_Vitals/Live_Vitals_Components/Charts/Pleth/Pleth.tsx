import React from 'react';
import Chart from 'react-apexcharts';
import { useState } from 'react';
import { IonCard, IonRow, IonCol, IonText } from '@ionic/react';
import { linecolor, data } from '../../../../../data/config.json'

import './Pleth.css'

const datas = data.map(x => x.data1);
console.log(datas)


const Pleth: React.FC = () => {

    const [options] = useState({
            chart: {
                id: 'realtime',
                toolbar: {
                    show: false
                },
            },
            yaxis: {
                tooltip: {
                    enabled: false
                },
                show: false,
                showAlways: false
            },
            xaxis: {
                tooltip: {
                    enabled: false
                },
                labels: {
                    show: false,
                },
            },
            grid: {
                show: false
            },
            legend: {
                show: false
            }
        })

    const [series] = useState(
        [{
            // name: 'series-1',
            // data: [30, 40, 35, 50, 49, 60, 70, 91, 125]
            data: datas[0],
            color: linecolor
        }],
    )
    return (
        <IonCard className="plethcontent">
            <IonRow>
                <IonCol>
                    <IonText>spO2</IonText>
                </IonCol>
                <IonCol>
                    <IonText className="ninety">97<span className="percent">%</span></IonText>
                </IonCol>
               
            </IonRow>
            <Chart options={options} series={series} type="line" height={80} />
        </IonCard>
    );
}
export default Pleth