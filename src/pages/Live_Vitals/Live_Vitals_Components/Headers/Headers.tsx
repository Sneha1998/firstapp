import { IonTitle, IonAvatar, IonGrid, IonCol, IonRow, IonIcon, IonButton, IonText, IonImg } from '@ionic/react';
import './Headers.css'
import { arrowBack,gift,nutrition} from 'ionicons/icons';

const Headers: React.FC = () => {
  return (
    <IonGrid className="grid">
      <IonRow>
        <IonCol size="1">
          <IonIcon icon={arrowBack}></IonIcon>
        </IonCol>
        <IonCol>
          <IonAvatar className="img">
            {/* <IonImg src="https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y" alt="img" /> */}
            <IonImg src="assets/images/img1.jpg" alt="img" />
          </IonAvatar>
        </IonCol>
        <IonCol>
          <IonTitle className="name">John Smith</IonTitle>
          <IonTitle className="covid">covid</IonTitle>
          <IonButton className="id" color="#5DD45D">VP431718</IonButton>
        </IonCol>
        <IonCol>
          <IonButton className="icons" color="#ffffff"><IonIcon icon={gift}></IonIcon>
          <IonIcon icon={nutrition}></IonIcon></IonButton>
        </IonCol>
      </IonRow>

      <IonRow>
        <IonCol className="btn">
          <IonButton className="live" color="#414040">Live</IonButton><IonButton className="historic" color="#FFFFFF">Historic</IonButton>
        </IonCol>
      </IonRow>

      <IonRow>
        <IonCol className="date1">
          <IonText className="date">Aug 16, 10.00 AM</IonText>
        </IonCol>
      </IonRow>
    </IonGrid>

  );
};

export default Headers;
