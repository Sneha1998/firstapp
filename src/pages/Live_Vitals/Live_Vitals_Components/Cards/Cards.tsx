import React from 'react';
import Card from '../../../../components/Live_Vital_Cards'
import { IonContent, IonGrid, IonRow, IonCol } from '@ionic/react';

const Cards = [
    {
        subtitle: "Pulse",
        img: "Title1",
        content: "99",
        text: "bpm"
    },
    {
        subtitle: "Temp",
        img: "Title2",
        content: "102",
        text: "F"
    },
    {
        subtitle: "Bp",
        img: "Title3",
        content: "120",
        text: "mmHg"
    }
]
const Card_Page: React.FC = () => {
    return (
        <IonContent>
            <IonGrid>
                <IonRow>
                    <IonCol size="4">
                        <Card subtitle={Cards[0]} />
                    </IonCol>
                    <IonCol size="4">
                        <Card subtitle={Cards[1]} />
                    </IonCol>
                    <IonCol size="4">
                        <Card subtitle={Cards[2]} />
                    </IonCol>
                </IonRow>
            </IonGrid>
        </IonContent>

    );
};
export default Card_Page;