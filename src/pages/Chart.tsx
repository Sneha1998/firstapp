// import { IonContent, IonPage } from '@ionic/react';
// import Highcharts from 'highcharts';
// import HighchartsReact from 'highcharts-react-official';

// const options = {
//     series: [
//     {
//         name:'Profit',
//         data:[100,200,30,100,30,50,100]
//     }
//     ]
// }
// const Chart: React.FC = () => {
//     return (
//         <IonPage>
//             <IonContent className="ion-padding">
//                 <HighchartsReact highcharts={Highcharts} options={options}/>
              
//             </IonContent>
//         </IonPage>

//     );
// };

// export default Chart;


//drilldown graph

import { IonContent, IonPage } from '@ionic/react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';
import drilldown from 'highcharts/modules/drilldown';


drilldown(Highcharts);

const options = {
    series: [{
        name: 'Things',
        colorByPoint: true,
        data: [{
            name: 'Animals',
            y: 5,
            drilldown: 'animals'
        }, {
            name: 'Fruits',
            y: 2,
            drilldown: 'fruits'
        }, {
            name: 'Cars',
            y: 4,
            drilldown: 'cars'
        }]
    }],
    drilldown: {
        series: [{
            id: 'animals',
            data: [
                ['Cats', 4],
                ['Dogs', 2],
                ['Cows', 1],
                ['Sheep', 2],
                ['Pigs', 1]
            ]
        }, {
            id: 'fruits',
            data: [
                ['Apples', 4],
                ['Oranges', 2]
            ]
        }, {
            id: 'cars',
            data: [
                ['Toyota', 4],
                ['Opel', 2],
                ['Volkswagen', 2]
            ]
        }]
    }     
}

const Chart: React.FC = () => {
    return (
        <IonPage>
            <IonContent className="ion-padding">
                <HighchartsReact highcharts={Highcharts} options={options} />
            </IonContent>
        </IonPage>

    );
};

export default Chart;



