
import { IonContent,  IonPage } from '@ionic/react';
import Area from '../components/Area'
const Charts: React.FC = () => {
    return (
        <IonPage>
            <IonContent className="ion-padding">
               <Area/>
            </IonContent>
        </IonPage>

    );
};

export default Charts;