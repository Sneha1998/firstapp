import { IonContent} from '@ionic/react';
import Charts from '../components/Charts'
import IonicCharts  from '../components/IonicChart';

const Charts_Ionic: React.FC = () => {
            return (
                <IonContent className="content">
                    <Charts />
                    <IonicCharts />
                </IonContent>
            )}
    
export default Charts_Ionic;