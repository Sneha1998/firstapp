import React from 'react';
import { Redirect, Route } from 'react-router-dom';

import {
    IonApp,
    IonIcon,
    IonLabel,
    IonPage,
    IonRouterOutlet,
    IonTabBar,
    IonTabButton,
    IonTabs
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import { apps, flash, send } from 'ionicons/icons';
import Tab1 from './Tabs/Tab1';
import Tab2 from './Tabs/Tab2';
import Tab3 from './Tabs/Tab3'


/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

const Categories: React.FunctionComponent = () => (
    <IonApp>
        <IonReactRouter>
            <IonPage>
                <IonTabs>
                    <IonRouterOutlet>
                        <Route path="/categories/:tab(tab1)" render={() => <Tab1/>} exact={true}/>
                        <Route path="/categories/:tab(tab2)" render={()=><Tab2/>} exact={true}/>
                        <Route path="/categories/:tab(tab3)" render={() => <Tab3 />} exact={true}/>
                        <Route exact path="/categories" render={() => <Redirect to="/categories/tab1" />} />
                    </IonRouterOutlet>
                    <IonTabBar slot="bottom">
                        <IonTabButton tab="/tab1" href="/categories/tab1">
                            <IonIcon icon={flash} />
                            <IonLabel>Tab One</IonLabel>
                        </IonTabButton>
                        <IonTabButton tab="tab2" href="/categories/tab2">
                            <IonIcon icon={apps} />
                            <IonLabel>Tab Two</IonLabel>
                        </IonTabButton>
                        <IonTabButton tab="tab3" href="/categories/tab3">
                            <IonIcon icon={send} />
                            <IonLabel>Tab Three</IonLabel>
                        </IonTabButton>
                    </IonTabBar>
                </IonTabs>
            </IonPage>
        </IonReactRouter>
    </IonApp>
);

export default Categories;
