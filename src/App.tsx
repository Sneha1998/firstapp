import {  Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './pages/Home';
import Login from './pages/Login'
import Register from './pages/Register'
import Categories from './pages/Categories';
import Card_Page from './pages/Card_Page'
import list from './pages/List'
import Charts from './pages/Charts'
import Chart from './pages/Chart'
// import Charts_Ionic from './pages/Charts_Ionic';
import Live_Vitals from './pages/Live_Vitals/Live_Vitals';



/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';


/* Theme variables */
import './theme/variables.css';

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <Route path="/" component={Home} exact />
        <Route path="/login" component={Login} exact />
        <Route path="/register" component={Register} exact/>
        <Route path="/categories" component={Categories} exact/>
        <Route path="/card" component={Card_Page} exact/>
        <Route path="/list" component={list} exact/>
        <Route path="/charts" component={Charts} exact/>
        <Route path="/chart" component={Chart} exact/>
        <Route path="/live_vitals" component={Live_Vitals} exact/>

        {/* <Route path="/Charts_Ionic" component={Charts_Ionic} exact/> */}
  
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
