import React, { ReactNode } from "react";
import { ResizableBox as ReactResizableBox } from "react-resizable";
import {
  IonContent
} from '@ionic/react';
// import "react-resizable/css/styles.css";


interface IProps {
    children: ReactNode;
}

export default function ResizableBox({ children }:IProps)
    {
    const resizable = true
    return (
        <IonContent>
        <div style={{ marginLeft: 20 }}>
            {resizable ? (
                <ReactResizableBox width={600} height={300}>
                    <div
                        style={{
                            boxShadow: "0 20px 40px rgba(0,0,0,.1)",
                            width: "100%",
                            height: "100%",
                        }}
                        className={''}
                    >
                        {children}
                    </div>
                </ReactResizableBox>
            ) : (
                <div
                    style={{
                        width: `${600}px`,
                        height: `${300}px`,
                        boxShadow: "0 20px 40px rgba(0,0,0,.1)",
                     
                    }}
                    className={''}
                >
                    {children}
                </div>
            )}
        </div>
        </IonContent>
    );
}